/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider;

import java.util.Map;
import java.util.Optional;

/**
 * Indicates that a token was verified as "not correct" by {@link TokenProvider#verifyToken(String)}.
 */
public class InvalidTokenException extends TokenProviderException {
    private static final long serialVersionUID = 717038249925246971L;

    public static enum Reason {
        /** The token signature is incorrect. */
        INVALID_SIGNATURE,
        /** The token has malformed claims. */
        MALFORMED_TOKEN,
        /** The token is expired. */
        TOKEN_EXPIRED,
        /** The token is not valid yet. */
        TOKEN_NOT_VALID,
        /** The token is invalid for unknown reasons. */
        UNKNOWN;
    }

    private final Reason m_reason;
    private final Map<String, String> m_attrs;

    /**
     * Constructs a new token provider exception.
     *
     * @param msg an error message providing more information about the cause of the error;
     */
    public InvalidTokenException(Reason reason, String msg, Map<String, String> attrs) {
        super(msg);
        m_reason = reason;
        m_attrs = attrs;
    }

    /**
     * Creates a new {@link InvalidTokenException} instance.
     *
     * @param msg an error message providing more information about the cause of the error;
     * @param t the originating cause of the error.
     */
    public InvalidTokenException(Reason reason, String msg, Throwable t, Map<String, String> attrs) {
        super(msg, t);
        m_reason = reason;
        m_attrs = attrs;
    }

    /**
     * @return the reason why the token was invalid, never <code>null</code>.
     */
    public Reason getReason() {
        return m_reason;
    }

    /**
     * @return the optional attributes that were "recovered" from the invalid token, can be an empty map.
     */
    public Optional<Map<String, String>> getAttributes() {
        return Optional.ofNullable(m_attrs);
    }
}
