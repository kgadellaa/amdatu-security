/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt.impl;

import static org.amdatu.security.tokenprovider.jwt.impl.ConfigUtils.getInteger;
import static org.amdatu.security.tokenprovider.jwt.impl.ConfigUtils.getMandatoryString;
import static org.amdatu.security.tokenprovider.jwt.impl.ConfigUtils.getMandatoryURI;
import static org.amdatu.security.tokenprovider.jwt.impl.ConfigUtils.getString;
import static org.amdatu.security.util.crypto.CryptoUtils.createSecureRandom;
import static org.amdatu.security.util.crypto.CryptoUtils.getHmacKey;
import static org.jose4j.jws.AlgorithmIdentifiers.HMAC_SHA256;
import static org.jose4j.jws.AlgorithmIdentifiers.HMAC_SHA384;
import static org.jose4j.jws.AlgorithmIdentifiers.HMAC_SHA512;

import java.io.IOException;
import java.net.URI;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.List;

import org.jose4j.keys.HmacKey;
import org.osgi.service.cm.ConfigurationException;

/**
 * Configuration used for the JWT-based token provider.
 */
public class TokenProviderConfig {
    /** The list of supported algorithms. */
    private static final List<String> ALGORITHMS = Arrays.asList(HMAC_SHA256, HMAC_SHA384, HMAC_SHA512);

    public static final String KEY_ALLOWED_CLOCK_SKEW = "allowedClockSkew";
    public static final String KEY_TOKEN_VALIDITY_PERIOD = "tokenValidityPeriod";
    public static final String KEY_ALGORITHM = "algorithm";
    public static final String KEY_AUDIENCE = "audience";
    public static final String KEY_ISSUER = "issuer";
    public static final String KEY_KEY_PASSWORD = "keyPassword";
    public static final String KEY_KEY_URI = "keyUri";
    public static final String KEY_PRNG_ALGORITHM = "secureRandomAlgorithm";

    private static final String DEFAULT_ALGORITHM = "HS256";
    private static final int DEFAULT_ALLOWED_CLOCK_SKEW = 10; // seconds.
    private static final int DEFAULT_TOKEN_VALIDITY_PERIOD = 3600; // seconds.

    private final String m_audience;
    private final String m_issuer;
    private final String m_algorithm;
    private final int m_allowedClockSkew;
    private final int m_tokenValidityPeriod;
    private final Key m_verificationKey;
    private final Key m_signingKey;

    public TokenProviderConfig() {
        m_issuer = m_audience = "Amdatu Token Provider";
        m_algorithm = HMAC_SHA256;
        m_allowedClockSkew = DEFAULT_ALLOWED_CLOCK_SKEW;
        m_tokenValidityPeriod = DEFAULT_TOKEN_VALIDITY_PERIOD;

        try {
            byte[] key = new byte[32];

            SecureRandom prng = createSecureRandom(null);
            prng.nextBytes(key);

            m_verificationKey = m_signingKey = new HmacKey(key);
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unable to get strong RNG!", e);
        }
    }

    /**
     * Creates a new {@link TokenProviderConfig} instance.
     */
    public TokenProviderConfig(Dictionary<String, ?> properties) throws ConfigurationException {
        m_tokenValidityPeriod = getInteger(properties, KEY_TOKEN_VALIDITY_PERIOD, DEFAULT_TOKEN_VALIDITY_PERIOD);
        m_allowedClockSkew = getInteger(properties, KEY_ALLOWED_CLOCK_SKEW, DEFAULT_ALLOWED_CLOCK_SKEW);
        m_algorithm = getString(properties, KEY_ALGORITHM, DEFAULT_ALGORITHM);
        m_audience = getMandatoryString(properties, KEY_AUDIENCE);
        m_issuer = getMandatoryString(properties, KEY_ISSUER);
        if (!ALGORITHMS.contains(m_algorithm)) {
            throw new ConfigurationException(KEY_ALGORITHM, "invalid algorithm!");
        }

        try {
            SecureRandom prng = createSecureRandom(getString(properties, KEY_PRNG_ALGORITHM, null));

            URI keyUri = getMandatoryURI(properties, KEY_KEY_URI);

            switch (m_algorithm) {
                case HMAC_SHA512:
                case HMAC_SHA384:
                case HMAC_SHA256: {
                    int keySize = getAlgorithmSize(m_algorithm);

                    m_verificationKey = m_signingKey = getHmacKey(prng, keyUri, keySize);
                    break;
                }
                // XXX extend with additional algorithms...
                default:
                    throw new ConfigurationException(KEY_ALGORITHM, "unsupported algorithm!");
            }
        }
        catch (NoSuchAlgorithmException e) {
            throw new ConfigurationException(KEY_ALGORITHM, "invalid algorithm!", e);
        }
        catch (IOException e) {
            throw new ConfigurationException(KEY_KEY_URI, "invalid file or resource!", e);
        }
    }

    /**
     * @return the JWA name of the used algorithm.
     */
    public String getAlgorithm() {
        return m_algorithm;
    }

    /**
     * @return the allowed clock skew, in seconds.
     */
    public int getAllowedClockSkew() {
        return m_allowedClockSkew;
    }

    /**
     * @return the intended audience of the token, for example, the client app that uses this provider.
     */
    public String getAudience() {
        return m_audience;
    }

    /**
     * @return the name of the token provider itself, for example, the web service that uses this provider.
     */
    public String getIssuer() {
        return m_issuer;
    }

    /**
     * @return the private signing key.
     */
    public Key getSigningKey() {
        return m_signingKey;
    }

    /**
     * @return the validity period of generated token, in seconds.
     */
    public int getTokenValidityPeriod() {
        return m_tokenValidityPeriod;
    }

    /**
     * @return the public verification key.
     */
    public Key getVerificationKey() {
        return m_verificationKey;
    }

    private static int getAlgorithmSize(String algorithm) throws NoSuchAlgorithmException {
        try {
            return Integer.valueOf(algorithm.substring(2));
        }
        catch (Exception e) {
            throw new NoSuchAlgorithmException("Invalid algorithm!", e);
        }
    }
}
