/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice;

import javax.servlet.http.HttpServletRequest;

/**
 * Useful constants used by this bundle.
 */
public interface AuthenticationConstants {

    /**
     * Attribute name on a {@link HttpServletRequest} that contains the type of the provider that authenticated a principal.
     */
    String REQ_PROVIDER_TYPE = "org.amdatu.security.authentication.provider.type";

    /**
     * Attribute name on a {@link HttpServletRequest} that contains the name of the provider that authenticated a principal.
     */
    String REQ_PROVIDER_NAME = "org.amdatu.security.authentication.provider.name";

    /**
     * Attribute name on a {@link HttpServletRequest} that contains error code in case of an authentication failure.
     */
    String REQ_ERROR_CODE = "org.amdatu.security.authentication.error.code";

    /**
     * Attribute name on a {@link HttpServletRequest} that contains error code in case of an authentication failure.
     */
    String REQ_LOCAL_ID = "org.amdatu.security.authentication.local.id";

    /**
     * Attribute name on a {@link HttpServletRequest} that contains error code in case of an authentication failure.
     */
    String REQ_ID_TOKEN = "org.amdatu.security.authentication.id.token";

}
