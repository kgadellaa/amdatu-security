/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import java.util.Collections;
import java.util.List;

public final class MockProtectedPerson {
    private final String m_identifier;

    public MockProtectedPerson(String identifier) {
        m_identifier = identifier;
    }

    public String getIdentifier() {
        return m_identifier;
    }

    public String getPersonId() {
        return "123456782";
    }

    public String getDriversLicenseId() {
        return "9876";
    }

    public List<Note> getNotes() {
        return Collections.singletonList(new Note());
    }

    public static class Note {
        public String getText() {
            return "Note";
        }
    }
}
