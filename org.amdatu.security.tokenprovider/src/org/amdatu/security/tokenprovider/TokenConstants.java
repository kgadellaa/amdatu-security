/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider;

/**
 * Common constants used by the {@link TokenProvider} and {@link TokenStorageProvider}.
 */
public interface TokenConstants {

    /**
     * The subject of a token, as used by Web Tokens to identify to subject for which the token was issued.
     */
    String SUBJECT = "sub";
    /**
     * The time on which the token was issued, given as number of seconds since the UNIX Epoch.
     */
    String ISSUED_AT = "iat";
    /**
     * The time when the token expires, given as number of seconds since the UNIX Epoch.
     */
    String EXPIRATION_TIME = "exp";
    /**
     * The time after which the token is valid, given as number of seconds since the UNIX Epoch.
     */
    String NOT_BEFORE = "nbf";

}
