'use strict';

var msgs = {
	'unknown_error': 'The request failed for unknown reasons.',
	'signup_success': 'You have successfully signed up.',
	'account_verified': 'Account is successfully verified.',
	'credentials_updated': 'Your credentials are successfully updated.',
	'reset_requested': 'Check your email to proceed your credential reset...',
};

function showErrorMessage() {
	var queryDict = {};
	location.search.substr(1).split("&").forEach(function(item) {
		queryDict[item.split("=")[0]] = item.split("=")[1]
	})

	var el = document.getElementById('error_msg')
	var err = queryDict['msg']
	if (err != undefined) {
		el.innerText = msgs[err] || msgs['unknown_error']
		el.style.display = 'block'
	} else {
		el.innerText = ''
		el.style.display = 'none'
	}
}

function updateFormElements(prefix, name, queryDict) {
	var el = document.getElementById(prefix + name)
	if (el) {
		el.value = decodeURIComponent(queryDict[name]) || ''
	}
}

function showRequestedForm() {
	var fragment = window.location.hash || '#signup'
	// strip off the hash sign...
	fragment = fragment.substring(1)

	var el = document.getElementById(fragment)
	if (el) {
	    el.style.display = 'block'
	}

	if (fragment === 'change') {
		var queryDict = {};
		location.search.substr(1).split("&").forEach(function(item) {
			queryDict[item.split("=")[0]] = item.split("=")[1]
		})

		updateFormElements('change_', 'resetToken', queryDict)
		updateFormElements('change_', 'email', queryDict)
	}
	else if (fragment === 'verify') {
        var queryDict = {};
        location.search.substr(1).split("&").forEach(function(item) {
            queryDict[item.split("=")[0]] = item.split("=")[1]
        })

        updateFormElements('verify_', 'token', queryDict)
        updateFormElements('verify_', 'email', queryDict)
    }
}

function bootstrapPage() {
	showRequestedForm()
	showErrorMessage()
}

document.addEventListener("DOMContentLoaded", bootstrapPage);

// EOF