/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest;

import static org.amdatu.security.account.admin.rest.AccountAdminHelper.*;
import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AdminUtilsTest {

    @Test
    public void testSplitCredentialsOk() throws Exception {
        Map<String, Object> newCreds = asMap("email", "foo@bar.com", "oldPassword", "old", "newPassword", "new");
        Map<String, Object> oldCreds = splitOldAndNewCredentials(newCreds);

        assertNotNull(oldCreds);
        assertEquals(2, oldCreds.size());
        assertEquals("foo@bar.com", oldCreds.get("email"));
        assertEquals("old", oldCreds.get("password"));

        assertEquals(2, newCreds.size());
        assertEquals("foo@bar.com", newCreds.get("email"));
        assertEquals("new", newCreds.get("password"));
    }

    @Test
    public void testSplitPartialCredentialsOk() throws Exception {
        Map<String, Object> newCreds = asMap("email", "foo@bar.com", "newPassword", "new", "oldFoo", "bar");
        Map<String, Object> oldCreds = splitOldAndNewCredentials(newCreds);

        assertNull(oldCreds);

        assertEquals(3, newCreds.size());
        assertEquals("foo@bar.com", newCreds.get("email"));
        assertEquals("new", newCreds.get("newPassword"));
        assertEquals("bar", newCreds.get("oldFoo"));
    }

    @Test
    public void testSplitCredentialsWithoutOldCredentialsOk() throws Exception {
        Map<String, Object> newCreds = asMap("email", "foo@bar.com", "accessToken", "someToken");
        Map<String, Object> oldCreds = splitOldAndNewCredentials(newCreds);

        assertNull(oldCreds);
    }

}
