/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides the ability to intercept and influence the authentication steps for a particular
 * realm.
 * <p>
 * Realms can have multiple interceptors, but typically, one interceptor is not shared amongst
 * realms (though the current implementation does not yet prevent you from trying this).
 * <p>
 * Interceptors are called in <b>reverse natural order</b>. This means that interceptors
 * with a higher service ranking are called before interceptors with a lower service ranking!
 * <p>
 * One should use service properties for the authentication interceptors registration to
 * allow binding of them to a particular realm. Each realm defines a "<tt>authInterceptorSelect</tt>"
 * configuration property that can be used to specify which interceptors should be used for a
 * particular realm. Each realm has a fixed set of "private" authentication interceptor
 * services that it uses. Those are registered with the service property "<tt>realm</tt>" set
 * to the name of the realm.
 */
public interface AuthenticationInterceptor {

    /**
     * Called before any of the actual authentication work is done.
     * <p>
     * This method is optional and by default does nothing.<br>
     * </p>
     *
     * @param request the HTTP servlet request;
     * @param response the HTTP servlet response.
     * @throws AuthenticationException in case of problems handling the given request and
     *         response. Throwing an exception causes this interceptor to fail, but other
     *         interceptors must be called as well.
     */
    default void onAuthenticationStart(HttpServletRequest request, HttpServletResponse response)
        throws AuthenticationException {
        // Nop
    }

    /**
     * Called after all of the actual authentication work is done.
     * <p>
     * This method is optional and by default does nothing.
     * </p>
     *
     * @param request the HTTP servlet request;
     * @param response the HTTP servlet response.
     * @throws AuthenticationException in case of problems handling the given request and
     *         response. Throwing an exception causes this interceptor to fail, but other
     *         interceptors must be called as well.
     */
    default void onAuthenticationEnd(HttpServletRequest request, HttpServletResponse response)
        throws AuthenticationException {
        // Nop
    }

    /**
     * Called whenever a user is logged out, either purposely or due to other reasons.
     *
     * @param context the authentication context, never <code>null</code>;
     * @param reason the reason as to why the logout happened, never <code>null</code>.
     * @throws AuthenticationException in case the logout cannot or should not happen.
     */
    void onLogout(AuthenticationContext context, LogoutReason reason) throws AuthenticationException;

    /**
     * Called upon a successful login of a user.
     *
     * @param context the authentication context, never <code>null</code>;
     * @param reason the reason as to why the login happened, never <code>null</code>.
     * @throws AuthenticationException in case the login cannot or should not happen. In
     *         this situation, the login procedure is aborted and the user is effectively
     *         denied access.
     */
    void onLogin(AuthenticationContext context, LoginReason reason) throws AuthenticationException;

    /**
     * Called when a user tried to login but failed for some reason.
     *
     * @param context the authentication context, never <code>null</code>;
     * @param reason the reason as to why the login was rejected, never <code>null</code>.
     */
    void onLoginRejected(AuthenticationContext context, RejectReason reason);

    public enum LoginReason {
            USER_REQUEST, TOKEN_REFRESHED,
    }

    public enum LogoutReason {
            USER_REQUEST, LOGIN_REQUIRED,
    }

    public enum RejectReason {
            INVALID_REQUEST, PROVIDER_ERROR, UNKNOWN_OR_INVALID_USER
    }
}
