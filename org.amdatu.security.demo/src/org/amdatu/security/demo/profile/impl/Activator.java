/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.demo.profile.impl;

import static org.amdatu.security.account.AccountConstants.TOPIC_PREFIX;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.security.authentication.authservice.PrincipalLookupService;
import org.amdatu.security.demo.profile.UserProfileService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.log.LogService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager dm) throws Exception {
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(EventConstants.EVENT_TOPIC, TOPIC_PREFIX.concat("/*"));
        props.put("principal.lookup", "demo");

        String[] ifaces = new String[] { EventHandler.class.getName(),
            UserProfileService.class.getName(), PrincipalLookupService.class.getName() };

        dm.add(createComponent()
            .setInterface(ifaces, props)
            .setImplementation(UserProfileServiceImpl.class)
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));
    }

}
