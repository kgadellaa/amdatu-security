/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.test.idprovider;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.web.testing.http.HttpTestConfigurator.createWaitForHttpEndpoints;
import static org.amdatu.web.testing.http.HttpTestConfigurator.useLocalCookieJar;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Desktop;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.amdatu.security.authentication.authservice.PrincipalLookupService;
import org.amdatu.security.authentication.test.app.MyAuthService;
import org.amdatu.security.authentication.test.app.MyLocalApp;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.github.kevinsawicki.http.HttpRequest;

/**
 * Test cases for OpenIdConnectProvider.
 */
public class OpenIdConnectProviderTest {
    public static final String[] KNOWN_USERS = {};

    private final MyAuthService m_authService = new MyAuthService(KNOWN_USERS);
    private final MyLocalApp m_app = new MyLocalApp();

    private String m_baseURI;

    @Before
    public void setUp() throws InterruptedException {
        Properties appProps = new Properties();
        appProps.put(JaxrsWhiteboardConstants.JAX_RS_RESOURCE, "true");

        int port = Integer.getInteger("org.osgi.service.http.port", 8080);
        m_baseURI = String.format("http://localhost:%d/", port);

        configure(this)
            .add(createComponent()
                .setInterface(Object.class.getName(), appProps)
                .setImplementation(m_app))
            .add(createComponent()
                .setInterface(PrincipalLookupService.class.getName(), null)
                .setImplementation(m_authService))
            .add(createFactoryConfiguration("org.amdatu.security.authentication.idprovider.openidconnect")
                .set("name", "google",
                    "clientId", "ID",
                    "clientSecret", "SECRET"))
            .add(createFactoryConfiguration("org.amdatu.security.authentication")
                .set("realm", "integration.test",
                    "osgi.http.whiteboard.context.name", "auth.test",
                    "osgi.http.whiteboard.context.path", "/authtest",
                    "idProviderFilter", "(&(providerType=google)(providerType=office))",
                    "preLoginURL", m_baseURI + "authtest/index.html",
                    "landingPageURL", m_baseURI + "rest/my/app",
                    "failureURL", m_baseURI + "rest/my/failure"))
            .add(createWaitForHttpEndpoints(m_baseURI + "authtest/rest/login/providers"))
            .add(useLocalCookieJar())
            .apply();

            // FIXME Tests are intermittently failing in the pipelines build, added a little pause before running the actual test
            Thread.sleep(1000);
    }

    @After
    public void tearDown() {
        cleanUp(this);
    }

    @Test
    public void testUseRemoteOpenIdConnectProviderOk() throws Exception {
        Assume.assumeTrue(Desktop.isDesktopSupported());
        Assume.assumeTrue(KNOWN_USERS.length > 0);

        Map<String, String> form = new HashMap<>();
        form.put("providerName", "google");

        HttpRequest loginPost = HttpRequest.post(m_baseURI + "authtest/rest/login").form(form).followRedirects(false);
        assertEquals(303, loginPost.code());

        Desktop.getDesktop().browse(URI.create(loginPost.location()));

        assertTrue(m_app.m_appCalled.await(30, TimeUnit.SECONDS));
        assertFalse(m_app.m_failureCalled.await(1, TimeUnit.MILLISECONDS));
    }
}
