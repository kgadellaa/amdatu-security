/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.amdatu.security.authorization.AuthorizationPolicy;
import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.AuthorizationRule;
import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.BreadAction;
import org.amdatu.security.authorization.EntityDescriptor;
import org.amdatu.security.authorization.EntityProvider;
import org.amdatu.security.authorization.EntitySelector;
import org.amdatu.security.authorization.OptionalBiPredicate;
import org.amdatu.security.authorization.builder.AuthorizationPolicyBuilder;
import org.amdatu.security.authorization.builder.AuthorizationRuleBuilder;
import org.amdatu.security.authorization.builder.EntityDescriptorBuilder;
import org.amdatu.security.authorization.builder.EntitySelectorBuilder;

public class MockProtectedPassportService implements AuthorizationPolicyProvider, EntityProvider<MockProtectedPassport> {

    private static final String CONTEXT = "Context";
    private static final String PASSPORT = "Passport";

    private volatile AuthorizationService m_authorizationService;

    @Override
    public Set<AuthorizationPolicy> getPolicies() {
        EntityDescriptor subjectDescriptor = EntityDescriptorBuilder.build().withAttributeQualifiers(MockContextProvider.getUserRoleQualifier("ADMIN")).done();
        EntityDescriptor resourceDescriptor = EntityDescriptorBuilder.buildTypeDescriptor(MockProtectedPassport.class);

        AuthorizationRule passportAccessRule = AuthorizationRuleBuilder.build()
            .withSubjectEntitySelectors(MockContextProvider.getContextSelector())
            .withResourceEntitySelectors(getPassportSelector())
            .withAuthorization(getAuthorization())
            .done();

        AuthorizationPolicy policy = AuthorizationPolicyBuilder.build()
            .forActions(EnumSet.allOf(BreadAction.class))
            .withSubjectDescriptor(subjectDescriptor)
            .withResourceDescriptor(resourceDescriptor)
            .withRules(passportAccessRule)
            .done();

        return Collections.singleton(policy);
    }

    private OptionalBiPredicate<Map<String, Object>, Map<String, Object>> getAuthorization() {
        return (subjectEntities, resourceEntities) -> {
            MockContext context = (MockContext) subjectEntities.get(CONTEXT);
            MockProtectedPassport passport = (MockProtectedPassport) resourceEntities.get(PASSPORT);
            MockProtectedJobRegistration registration = context.get(MockProtectedJobRegistration.class);
            String userOrganization = context.get(MockContext.USER_ORGANIZATION_KEY);

            if (userOrganization.equals(registration.getCompanyIdentifier()) && registration.getPassportId().equals(passport.getIdentifier())) {
                return Optional.of(true);
            }
            return Optional.of(false);
        };
    }

    private EntitySelector getPassportSelector() {
        return EntitySelectorBuilder.build()
            .withType(MockProtectedPassport.class.getName())
            .withKey(PASSPORT)
            .done();
    }

    public MockProtectedPassport getPassport(String passportIdentifier) {
        Map<String, Object> subjectProperties = new HashMap<>();
        subjectProperties.put(AuthorizationService.SUBJECT_IDENTIFIER, MockContext.get().getUserUUID());

        Map<String, Object> resourceProperties = new HashMap<>();
        resourceProperties.put(AuthorizationService.RESOURCE_IDENTIFIER, passportIdentifier);

        if (m_authorizationService.isAuthorized("user", subjectProperties, BreadAction.READ, MockProtectedPassport.class.getName(), Optional.of(resourceProperties))) {
            return getPassportUnprotected(passportIdentifier);
        }
        throw new RuntimeException("No access");
    }

    private MockProtectedPassport getPassportUnprotected(String passportId) {
        return new MockProtectedPassport(passportId);
    }

    @Override
    public String getEntityType() {
        return MockProtectedPassport.class.getName();
    }

    @Override
    public Collection<String> getRequiredPropertyKeys() {
        return Collections.singleton(AuthorizationService.RESOURCE_IDENTIFIER);
    }

    @Override
    public Optional<MockProtectedPassport> getEntity(Map<String, Object> properties) {
        return Optional.ofNullable(getPassportUnprotected((String) properties.get(AuthorizationService.RESOURCE_IDENTIFIER)));
    }
}
