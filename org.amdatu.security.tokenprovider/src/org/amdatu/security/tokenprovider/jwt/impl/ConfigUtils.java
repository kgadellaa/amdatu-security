/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.service.cm.ConfigurationException;

/**
 * Provides various utility methods for use with a configuration {@link Dictionary}.
 */
class ConfigUtils {

    public static Dictionary<String, Object> asDictionary(Object... entries) {
        if (entries == null || (entries.length % 2) != 0) {
            throw new IllegalArgumentException("Need an even number of arguments!");
        }

        Hashtable<String, Object> result = new Hashtable<>();
        for (int i = 0, size = entries.length; i < size; i += 2) {
            String key = entries[i].toString();
            Object val = entries[i + 1];
            if (val != null) {
                result.put(key, val);
            }
        }
        return result;
    }

    static int getInteger(Dictionary<String, ?> properties, String key, int dflt) throws ConfigurationException {
        Object val = properties.get(key);
        if (val == null) {
            return dflt;
        }
        if (val instanceof Number) {
            return ((Number) val).intValue();
        }
        try {
            return Integer.valueOf(val.toString());
        }
        catch (NumberFormatException e) {
            throw new ConfigurationException(key, "invalid value!", e);
        }
    }

    static int getMandatoryInteger(Dictionary<String, ?> properties, String key) throws ConfigurationException {
        Object val = properties.get(key);
        if (val == null) {
            throw new ConfigurationException(key, "missing value!");
        }
        if (val instanceof Number) {
            return ((Number) val).intValue();
        }
        try {
            return Integer.valueOf(val.toString());
        }
        catch (NumberFormatException e) {
            throw new ConfigurationException(key, "invalid value!", e);
        }
    }

    static String getMandatoryString(Dictionary<String, ?> properties, String key) throws ConfigurationException {
        String val = getString(properties, key, null);
        if (val == null) {
            throw new ConfigurationException(key, "missing or invalid value!");
        }
        return val;
    }

    static URI getMandatoryURI(Dictionary<String, ?> properties, String key) throws ConfigurationException {
        Object val = properties.get(key);
        if (val == null || "".equals(val)) {
            throw new ConfigurationException(key, "missing or invalid value!");
        }

        try {
            return new URI(val.toString());
        }
        catch (URISyntaxException e) {
            throw new ConfigurationException(key, "invalid URI!", e);
        }
    }

    static String getString(Dictionary<String, ?> properties, String key, String dflt) throws ConfigurationException {
        Object val = properties.get(key);
        if (val == null || "".equals(val)) {
            return dflt;
        }
        return val.toString();
    }
}
