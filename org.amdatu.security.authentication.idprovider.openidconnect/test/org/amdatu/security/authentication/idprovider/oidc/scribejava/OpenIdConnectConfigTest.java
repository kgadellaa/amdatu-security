/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc.scribejava;

import static org.amdatu.security.authentication.idprovider.oidc.OpenIdConnectProviderConfig.GOOGLE_OPENID_CONF;
import static org.amdatu.security.authentication.idprovider.oidc.OpenIdConnectProviderConfig.OFFICE_OPENID_CONF;
import static org.amdatu.security.authentication.idprovider.oidc.scribejava.OpenIdConnectConfig.parseOICConfig;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test cases for {@link OpenIdConnectConfig}.
 */
public class OpenIdConnectConfigTest {
    private static final byte[] OIC_CFG = ("{\"authorization_endpoint\":\"https://localhost:8080/oauth2/authorize\","
        + "\"token_endpoint\":\"https://localhost:8080/oauth2/token\",\"token_endpoint_auth_methods_supported\":"
        + "[\"client_secret_post\",\"private_key_jwt\"],\"jwks_uri\":\"https://localhost:8080/discovery/keys\","
        + "\"response_modes_supported\":[\"query\",\"fragment\",\"form_post\"],\"subject_types_supported\":"
        + "[\"pairwise\"],\"id_token_signing_alg_values_supported\":[\"RS256\"],\"http_logout_supported\":true,"
        + "\"response_types_supported\":[\"code\",\"id_token\",\"code id_token\",\"token id_token\",\"token\"],"
        + "\"scopes_supported\":[\"openid\"],\"issuer\":\"https://localhost:8080/oauth2/{tenantid}/\","
        + "\"claims_supported\":[\"sub\",\"iss\",\"aud\",\"exp\",\"iat\",\"auth_time\",\"acr\",\"amr\",\"nonce\","
        + "\"email\",\"given_name\",\"family_name\",\"nickname\"],\"microsoft_multi_refresh_token\":true,"
        + "\"check_session_iframe\":\"https://localhost:8080/oauth2/checksession\",\"end_session_endpoint\":"
        + "\"https://localhost:8080/oauth2/logout\",\"userinfo_endpoint\":"
        + "\"https://localhost:8080/openid/userinfo\",\"claims_parameter_supported\":true}").getBytes();

    private static void assertListEquals(List<String> input, String... expected) {
        Assert.assertArrayEquals(expected, input.toArray(new String[input.size()]));
    }

    @Test
    public void testParseRemoteGoogleOpenIdConnectConfiguration() throws Exception {
        OpenIdConnectConfig cfg = parseOICConfig(URI.create(GOOGLE_OPENID_CONF));
        assertNotNull(cfg);

        assertTrue(cfg.isValid());

        assertEquals("https://accounts.google.com", cfg.getIssuer());
        assertEquals("https://accounts.google.com/o/oauth2/v2/auth", cfg.getAuthorizationEndpoint());
        assertEquals("https://www.googleapis.com/oauth2/v4/token", cfg.getTokenEndpoint());
        assertListEquals(cfg.getScopesSupported(), "openid", "email", "profile");
    }

    // Ignored as the endpoint for the configuration is rate-limited and therefore can refuse our
    // connection in case we're running this test too frequently...
    @Test
    @Ignore
    public void testParseRemoteOfficeOpenIdConnectConfiguration() throws Exception {
        OpenIdConnectConfig cfg = parseOICConfig(URI.create(OFFICE_OPENID_CONF));
        assertNotNull(cfg);

        assertTrue(cfg.isValid());

        assertEquals("https://sts.windows.net/{tenantid}/", cfg.getIssuer());
        assertEquals("https://login.microsoftonline.com/common/oauth2/authorize", cfg.getAuthorizationEndpoint());
        assertEquals("https://login.microsoftonline.com/common/oauth2/token", cfg.getTokenEndpoint());
        assertListEquals(cfg.getScopesSupported(), "openid");
    }

    @Test
    public void testParseStaticOpenIdConnectConfiguration() throws Exception {
        OpenIdConnectConfig cfg = parseOICConfig(new ByteArrayInputStream(OIC_CFG));
        assertNotNull(cfg);

        assertTrue(cfg.isValid());

        assertEquals("https://localhost:8080/oauth2/authorize", cfg.getAuthorizationEndpoint());
        assertEquals("https://localhost:8080/oauth2/token", cfg.getTokenEndpoint());
        assertListEquals(cfg.getScopesSupported(), "openid");
        assertListEquals(cfg.getClaimsSupported(), "sub", "iss", "aud", "exp", "iat", "auth_time", "acr", "amr",
            "nonce", "email",
            "given_name", "family_name", "nickname");
        assertTrue(cfg.isClaimsParameterSupported());
    }
}
