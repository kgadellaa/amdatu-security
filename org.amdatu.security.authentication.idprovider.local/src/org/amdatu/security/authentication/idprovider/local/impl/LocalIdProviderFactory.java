/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.local.impl;

import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_NAME;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_TYPE;
import static org.amdatu.security.authentication.idprovider.local.impl.LocalIdProviderConfig.TYPE_LOCAL;
import static org.amdatu.security.authentication.idprovider.local.impl.util.ConfigUtils.asDictionary;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.util.Dictionary;

import org.amdatu.security.authentication.idprovider.IdProvider;
import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.log.LogService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class LocalIdProviderFactory {

    // Managed by Felix DM...
    private volatile DependencyManager m_dm;
    private volatile LogService m_log;
    // Locally managed...
    private volatile Component m_idProviderComp;

    /**
     * Called by Felix DM.
     */
    public final void updated(Dictionary<String, ?> properties) throws Exception {
        LocalIdProviderConfig config = new LocalIdProviderConfig(properties);

        if (removeIdProvider()) {
            log(LOG_INFO, "Destroyed local identity provider '%s'...", config.getName());
        }

        createIdProvider(config);

        log(LOG_INFO, "Created local identity provider '%s'...", config.getName());
    }

    private void createIdProvider(LocalIdProviderConfig config) throws InterruptedException {
        LocalIdProvider idProvider = new LocalIdProvider(config);

        m_idProviderComp = m_dm.createComponent()
            .setInterface(IdProvider.class.getName(), asDictionary(
                PROVIDER_TYPE, TYPE_LOCAL,
                PROVIDER_NAME, config.getName()))
            .setImplementation(idProvider)
            .add(m_dm.createServiceDependency()
                .setService(LocalIdCredentialsProvider.class, config.getIdCredentialProviderFilter())
                .setRequired(true))
            .add(m_dm.createServiceDependency()
                .setService(LogService.class)
                .setRequired(false));

        m_dm.add(m_idProviderComp);
    }

    protected final void destroy(Component thisComp) throws Exception {
        removeIdProvider();
    }

    private boolean removeIdProvider() {
        boolean result = false;
        if (m_idProviderComp != null) {
            m_dm.remove(m_idProviderComp);
            m_idProviderComp = null;
            result = true;
        }
        return result;
    }

    private void log(int level, String msg, Object... args) {
        if (m_log != null) {
            m_log.log(level, String.format(msg, args));
        }
    }

}
