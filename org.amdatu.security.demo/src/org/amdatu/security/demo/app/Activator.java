/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.demo.app;

import static java.lang.String.format;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.ws.rs.core.Application;

import org.amdatu.security.authentication.authservice.AuthenticationHandler;
import org.amdatu.security.demo.profile.UserProfileService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager dm) {
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(JAX_RS_RESOURCE, "true");
        props.put(JAX_RS_APPLICATION_SELECT, format("(%s=%s)", JAX_RS_NAME, "my_app"));

        dm.add(createComponent()
            .setInterface(Object.class.getName(), props)
            .setImplementation(SecuredResource.class)
            .add(createServiceDependency().setService(UserProfileService.class).setRequired(true))
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));

        props = new Hashtable<>();
        props.put(JAX_RS_APPLICATION_BASE, "/rest");
        props.put(JAX_RS_APPLICATION_CONTEXT, "my_app");
        props.put(JAX_RS_NAME, "my_app");

        dm.add(createComponent()
            .setInterface(Application.class.getName(), props)
            .setImplementation(SecuredApplication.class));

        props = new Hashtable<>();
        props.put("org.amdatu.web.wink.rest.path", "/rest");
        props.put(HTTP_WHITEBOARD_CONTEXT_PATH, "/app");
        props.put(HTTP_WHITEBOARD_CONTEXT_NAME, "my_app");

        dm.add(createComponent()
            .setInterface(ServletContextHelper.class.getName(), props)
            .setImplementation(SecuredResourceContextHelper.class)
            .add(createServiceDependency().setService(AuthenticationHandler.class).setRequired(true))
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));

        props = new Hashtable<>();
        props.put(HTTP_WHITEBOARD_RESOURCE_PREFIX, "/res");
        props.put(HTTP_WHITEBOARD_RESOURCE_PATTERN, "/*");
        props.put(HTTP_WHITEBOARD_CONTEXT_SELECT, "(osgi.http.whiteboard.context.name=my_app)");

        dm.add(createComponent()
            .setInterface(Object.class.getName(), props)
            .setImplementation(new Object()));
    }

}
