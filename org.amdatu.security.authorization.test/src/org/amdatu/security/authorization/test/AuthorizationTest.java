/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.amdatu.security.authorization.Action;
import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.BreadAction;
import org.junit.Test;

import junit.framework.TestCase;

public class AuthorizationTest extends TestCase {

    private volatile AuthorizationService m_authorizationService;
    private volatile MockProtectedPassportService m_passportService;
    private volatile MockProtectedPersonService m_personService;

    @Override
    public void setUp() throws Exception {
        configure(this)
            .add(createServiceDependency().setService(AuthorizationService.class).setRequired(true))
            .add(createServiceDependency().setService(MockProtectedPassportService.class).setRequired(true))
            .add(createServiceDependency().setService(MockProtectedPersonService.class).setRequired(true))
            .apply();
    }

    @Override
    protected void tearDown() throws Exception {
        cleanUp(this);
    }

    @Test
    public void testAdminProcessingRegistration() {
        MockContext context = MockContext.get();

        // user selecting a job registration to process
        MockProtectedJobRegistration registration = new MockProtectedJobRegistration("xxyyzz");
        MockContext.get().put(MockProtectedJobRegistration.class, registration);

        // correct role and organization
        context.put(MockContext.USER_ROLE_KEY, "ADMIN");
        context.put(MockContext.USER_ORGANIZATION_KEY, "9876");
        try {
            Map<String, Object> subjectProperties = new HashMap<>();
            Map<String, Object> resourceProperties = new HashMap<>();

            resourceProperties.put(AuthorizationService.RESOURCE_IDENTIFIER, registration.getPassportId());

            Set<Action> authorizedActions = m_authorizationService.getAuthorizedActions("dummyType", subjectProperties, MockProtectedPassport.class.getName(), Optional.of(resourceProperties));
            assertEquals(EnumSet.allOf(BreadAction.class), authorizedActions);

            m_passportService.getPassport(registration.getPassportId());
        }
        finally {
            context.clear();
        }

        // right role, wrong organization = policy applicable, rule returns false
        context.put(MockProtectedJobRegistration.class, registration);
        context.put(MockContext.USER_ROLE_KEY, "ADMIN");
        context.put(MockContext.USER_ORGANIZATION_KEY, "7654");
        try {
            m_passportService.getPassport(registration.getPassportId());
            fail("Exception expected");
        }
        catch (RuntimeException e) {
            assertEquals("No access", e.getMessage());
        }
        finally {
            context.clear();
        }

        // wrong role, right organization = policy not applicable, rule not evaluated
        context.put(MockProtectedJobRegistration.class, registration);
        context.put(MockContext.USER_ROLE_KEY, "HELPDESK");
        context.put(MockContext.USER_ORGANIZATION_KEY, "9876");
        try {
            m_passportService.getPassport(registration.getPassportId());
            fail("Exception expected");
        }
        catch (RuntimeException e) {
            assertEquals("No access", e.getMessage());
        }
        finally {
            context.clear();
        }
    }

    @Test
    public void testUserReadsPersonNote() {
        MockContext context = MockContext.get();

        Person petra = new Person("petra");
        context.put(MockContext.USER_ASSOCIATED_PERSON_KEY, petra);

        // right role, right organization
        context.put(MockContext.USER_ROLE_KEY, "USER");
        context.put(MockContext.USER_ORGANIZATION_KEY, "9876");
        try {
            m_personService.getPerson("xxyyzz");
        }
        finally {
            context.clear();
        }

        // right role, wrong organization = policy applicable, rule returns false
        context.put(MockContext.USER_ROLE_KEY, "USER");
        context.put(MockContext.USER_ORGANIZATION_KEY, "7654");
        try {
            m_personService.getPerson("xxyyzz");
            fail("Exception expected");
        }
        catch (RuntimeException e) {
            assertEquals("No access", e.getMessage());
        }
        finally {
            context.clear();
        }

        // wrong role, right organization = policy not applicable, rule not evaluated
        context.put(MockContext.USER_ROLE_KEY, "GUEST");
        context.put(MockContext.USER_ORGANIZATION_KEY, "9876");
        try {
            m_personService.getPerson("xxyyzz");
            fail("Exception expected");
        }
        catch (RuntimeException e) {
            assertEquals("No access", e.getMessage());
        }
        finally {
            context.clear();
        }
    }
}
