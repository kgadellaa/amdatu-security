/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest;

import static java.lang.String.format;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.osgi.framework.Constants.SERVICE_RANKING;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Objects;
import java.util.Optional;

import javax.ws.rs.core.Application;

import org.amdatu.security.authentication.authservice.AuthenticationHandler;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor;
import org.amdatu.security.authentication.authservice.PrincipalLookupService;
import org.amdatu.security.authentication.idprovider.IdProvider;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.log.LogService;

/**
 * Controller service that registers an context, application and REST resource for any configured realm.
 */
public class AuthenticationRealm {
    public static final String AUTH_REALM = "realm";
    public static final String AUTH_INTERCEPTOR = "auth.interceptor";
    /**
     * Default service ranking for authentication token cookie interceptor, allowing
     * people to register one before or after it to give it precedence.
     */
    private static final int AUTH_INTERCEPTOR_SERVICE_RANKING = 10;

    // Injected by Felix DM...
    private volatile DependencyManager m_dm;
    private volatile LogService m_log;
    // Locally managed...
    private Component m_resComp;
    private Component m_ctxComp;
    private Component m_appComp;
    private Component m_authTokenComp;

    private static Dictionary<String, Object> asDictionary(Object... args) {
        Dictionary<String, Object> result = new Hashtable<>();
        for (int i = 0; i < args.length; i += 2) {
            result.put(Objects.toString(args[i]), args[i + 1]);
        }
        return result;
    }

    private static String coerceToNull(String input) {
        if (input == null) {
            return input;
        }
        input = input.trim();
        return "".equals(input) ? null : input;
    }

    private static Dictionary<String, Object> createAppServiceProps(AuthenticationRealmConfig config) {
        String realm = config.getRealm();

        return asDictionary(
            AUTH_REALM, realm,
                JAX_RS_APPLICATION_BASE, "/rest",
                JAX_RS_APPLICATION_CONTEXT, config.getContextName(),
                JAX_RS_NAME, realm);
    }

    private static Dictionary<String, Object> createContextServiceProps(AuthenticationRealmConfig config) {

        Dictionary<String, Object> ctxProps = asDictionary(
            AUTH_REALM, config.getRealm(),
            HTTP_WHITEBOARD_CONTEXT_NAME, config.getContextName(),
            HTTP_WHITEBOARD_CONTEXT_PATH, config.getContextPath());

        Optional.ofNullable(config.getHttpWhiteboardTarget())
            .map(AuthenticationRealm::coerceToNull)
            .ifPresent(filter -> ctxProps.put(HTTP_WHITEBOARD_TARGET, filter));

        return ctxProps;
    }

    private static Dictionary<String, Object> createInterceptorServiceProps(AuthenticationRealmConfig config) {
        return asDictionary(SERVICE_RANKING, AUTH_INTERCEPTOR_SERVICE_RANKING,
            AUTH_REALM, config.getRealm(),
            AUTH_INTERCEPTOR, config.getRealm());
    }

    private static Dictionary<String, Object> createResServiceProps(AuthenticationRealmConfig config) {
        String realm = config.getRealm();
        return asDictionary(AUTH_REALM, realm,
                JAX_RS_RESOURCE, true,
                JAX_RS_APPLICATION_SELECT, format("(%s=%s)", JAX_RS_NAME, realm));
    }

    /**
     * Called by Felix DM for each configuration change.
     */
    public void updated(Dictionary<String, ?> properties) throws Exception {
        AuthenticationRealmConfig config = new AuthenticationRealmConfig(properties);

        if (removeRealmComponents()) {
            log(LOG_INFO, "Destroyed realm '%s'", config.getRealm());
        }

        createRealmComponents(config);

        log(LOG_INFO, "Created realm '%s'", config.getRealm());
    }

    /**
     * Called by Felix DM upon deletion/destruction of this component.
     */
    protected final void destroy(Component thisComp) {
        removeRealmComponents();
    }

    private void createRealmComponents(AuthenticationRealmConfig config) {
        AuthenticationTokenCookieInterceptor interceptor = new AuthenticationTokenCookieInterceptor(config);
        AuthenticationServletContext ctx = new AuthenticationServletContext(config);
        AuthenticationApplication app = new AuthenticationApplication();
        AuthenticationResource res = new AuthenticationResource(config);

        m_authTokenComp = m_dm.createComponent()
            .setInterface(AuthenticationInterceptor.class.getName(), createInterceptorServiceProps(config))
            .setImplementation(interceptor)
            .add(m_dm.createServiceDependency().setService(TokenProvider.class)
                .setRequired(true))
            .add(m_dm.createServiceDependency().setService(LogService.class)
                .setRequired(false));

        m_resComp = m_dm.createComponent()
            .setInterface(AuthenticationHandler.class.getName(), createResServiceProps(config))
            .setImplementation(res)
            .add(m_dm.createServiceDependency()
                .setService(PrincipalLookupService.class, config.getPrincipalLookupServiceFilter())
                .setRequired(true))
            .add(m_dm.createServiceDependency()
                .setService(IdProvider.class, config.getIdProviderFilter())
                .setCallbacks("addIdProvider", "removeIdProvider"))
            .add(m_dm.createServiceDependency()
                .setRequired(true)
                .setService(AuthenticationInterceptor.class, format("(%s=%s)", AUTH_INTERCEPTOR, config.getRealm()))
                .setCallbacks("addAuthInterceptor", "removeAuthInterceptor"))
            .add(m_dm.createServiceDependency().setService(LogService.class)
                .setRequired(false));

        // Add configured interceptors as service dependency
        for (String interceptorFilter : config.getAuthenticationInterceptorSelectFilter()) {
            m_resComp.add(m_dm.createServiceDependency()
                .setRequired(true)
                .setService(AuthenticationInterceptor.class, interceptorFilter)
                .setCallbacks("addAuthInterceptor", "removeAuthInterceptor"));
        }


        m_ctxComp = m_dm.createComponent()
            .setInterface(ServletContextHelper.class.getName(), createContextServiceProps(config))
            .setImplementation(ctx);

        m_appComp = m_dm.createComponent()
            .setInterface(Application.class.getName(), createAppServiceProps(config))
            .setImplementation(app);

        m_dm.add(m_authTokenComp);
        m_dm.add(m_appComp);
        m_dm.add(m_ctxComp);
        m_dm.add(m_resComp);
    }

    private void log(int level, String msg, Object... args) {
        if (m_log != null) {
            m_log.log(level, format(msg, args));
        }
    }

    private boolean removeRealmComponents() {
        boolean result = false;
        if (m_appComp != null) {
            m_dm.remove(m_appComp);
            m_appComp = null;
            result = true;
        }
        if (m_ctxComp != null) {
            m_dm.remove(m_ctxComp);
            m_ctxComp = null;
            result = true;
        }
        if (m_resComp != null) {
            m_dm.remove(m_resComp);
            m_resComp = null;
            result = true;
        }
        if (m_authTokenComp != null) {
            m_dm.remove(m_authTokenComp);
            m_authTokenComp = null;
            result = true;
        }
        return result;
    }
}
