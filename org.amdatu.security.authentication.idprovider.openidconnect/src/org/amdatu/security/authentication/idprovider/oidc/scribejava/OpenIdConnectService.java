/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc.scribejava;

import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toMap;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.oauth.OAuth20Service;

/**
 * Provides a OpenId connect specific authentication service implementation.
 */
public class OpenIdConnectService extends OAuth20Service {

    private final OpenIdConnectApi m_api;

    /**
     * Creates a new {@link OpenIdConnectService} instance.
     */
    public OpenIdConnectService(OpenIdConnectApi api, OAuthConfig config) {
        super(api, config);

        m_api = api;
    }

    public final Map<String, String> getUserInfo(String accessToken)
        throws IOException, InterruptedException, ExecutionException {
        OAuthRequest request = createUserInfoRequest(accessToken);

        return sendUserInfoRequestSync(request);
    }

    protected OAuthRequest createUserInfoRequest(String accessToken) {
        if (accessToken == null || accessToken.isEmpty()) {
            throw new IllegalArgumentException("The accessToken cannot be null or empty");
        }
        OAuthRequest request = new OAuthRequest(m_api.getUserInfoVerb(), m_api.getUserInfoEndpoint());

        m_api.getSignatureType().signRequest(accessToken, request);

        request.addHeader("Accept", "application/json");

        return request;
    }

    protected Map<String, String> sendUserInfoRequestSync(OAuthRequest request)
        throws IOException, InterruptedException, ExecutionException {
        Response response = execute(request);
        if (!response.isSuccessful()) {
            return emptyMap();
        }

        try {
            return flattenMap(new JSONParser().parse(response.getBody()));
        }
        catch (ParseException e) {
            throw new IOException("Unable to parse user info!", e);
        }
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> flattenMap(Object val) {
        return ((Map<String, Object>) val).entrySet().stream()
            .collect(toMap(e -> e.getKey(), e -> e.getValue().toString()));
    }
}
