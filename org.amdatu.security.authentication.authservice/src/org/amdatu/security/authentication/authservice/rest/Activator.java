/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
    private static final String FACTORY_PID = "org.amdatu.security.authentication";

    @Override
    public void init(BundleContext context, DependencyManager dm) throws Exception {
        dm.add(createFactoryConfigurationAdapterService(FACTORY_PID, "updated", false /* propagate */)
            .setImplementation(AuthenticationRealm.class)
            .setAutoConfig(Component.class, false)
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));
    }
}
