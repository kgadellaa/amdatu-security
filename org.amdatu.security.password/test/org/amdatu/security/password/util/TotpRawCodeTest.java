/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.util;

import static org.junit.Assert.assertTrue;

import java.time.Instant;

import org.junit.Test;

/**
 * Test cases for {@link Totp} raw codes.
 */
public class TotpRawCodeTest {

    /**
     * Tests that the verification of raw codes is lenient wrt to the time at which the
     * verification takes place: due to the division of the timestamp with the timestep parameter,
     * we can get subtle rounding differences that can cause the verification to fail. The solution
     * is to test for the previous and next time-period as well.
     */
    @Test
    public void testVerifyRawCodesOverTimePeriodOk() throws Exception {
        byte[] secret = new byte[64];
        byte[] challenge = new byte[32];

        int timestep = 30;

        Totp totp = new Totp.Builder().setTimeStep(timestep).setSecret(secret).build();

        Instant now = Instant.now();

        byte[] code = totp.generateRawCode(now.getEpochSecond(), challenge, null);

        // Simulate small timing differences between [-30..30] when verifying the raw code...
        for (int i = -(timestep * 1000); i < (timestep * 1000); i += 50) {
            assertTrue(String.format("Verify with %d ms difference failed!", i),
                totp.validateRawCode(now.plusMillis(i).getEpochSecond(), code, challenge));
        }
    }

}
