/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Represents a <em>local</em> account that is managed by the {@link AccountAdmin} and used in the local identity provider.
 */
public class Account {

    public static enum State {
            /** Account is verified and can be used normally. */
            NORMAL,
            /** Account is created, but not yet verified by its owner. */
            ACCOUNT_VERIFICATION_NEEDED,
            /** The owner has requested to reset the credentials. */
            VOLUNTARY_CREDENTIALS_RESET,
            /** The owner or admin has requested to reset the credentials which <b>must</b> be changed prior to being able to use the account again. */
            FORCED_CREDENTIALS_RESET;
    }

    private final String m_id;
    private final Map<String, String> m_credentials;
    private State m_state;
    private boolean m_locked;
    private String m_accessToken;

    /**
     * Creates a new {@link Account} instance.
     */
    public Account(Account account) {
        m_id = account.getId();
        m_credentials = new HashMap<>(account.getCredentials());
        m_state = account.getState();
        m_locked = account.isLocked();
        account.getAccessToken().ifPresent(token -> m_accessToken = token);
    }

    /**
     * Creates a new {@link Account} instance.
     */
    public Account(String id, Map<String, String> credentials, State state, String accessToken, boolean locked) {
        m_id = Objects.requireNonNull(id);
        m_credentials = Objects.requireNonNull(credentials);
        m_state = Objects.requireNonNull(state);
        m_accessToken = accessToken;
        m_locked = locked;
    }

    /**
     * Clears the access token and marks the state of this account as normal.
     */
    public void clearAccessToken() {
        assertNotLocked();

        m_state = State.NORMAL;
        m_accessToken = null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Account other = (Account) obj;
        if (!m_id.equals(other.m_id)) {
            return false;
        }
        if (m_state != other.m_state) {
            return false;
        }
        if (m_locked != other.m_locked) {
            return false;
        }
        if (!m_credentials.equals(other.m_credentials)) {
            return false;
        }
        if (m_accessToken == null) {
            if (other.m_accessToken != null) {
                return false;
            }
        }
        else if (!m_accessToken.equals(other.m_accessToken)) {
            return false;
        }
        return true;
    }

    /**
     * @return the credentials of this account, cannot be <code>null</code>.
     */
    public Map<String, String> getCredentials() {
        return Collections.unmodifiableMap(m_credentials);
    }

    /**
     * @return the unique identifier of this account, cannot be <code>null</code>.
     */
    public String getId() {
        return m_id;
    }

    /**
     * @return the optional access token, needed to access the credentials in case of a credential-reset or unverified account.
     */
    public Optional<String> getAccessToken() {
        return Optional.ofNullable(m_accessToken);
    }

    /**
     * @return the state of this account, never <code>null</code>.
     */
    public State getState() {
        return m_state;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + m_id.hashCode();
        result = prime * result + m_state.hashCode();
        result = prime * result + m_credentials.hashCode();
        result = prime * result + (m_locked ? 1231 : 1237);
        result = prime * result + ((m_accessToken == null) ? 0 : m_accessToken.hashCode());
        return result;
    }

    /**
     * @return <code>true</code> if the needs to be verified, <code>false</code> otherwise.
     */
    public boolean isAccountVerificationNeeded() {
        return m_state == State.ACCOUNT_VERIFICATION_NEEDED;
    }

    /**
     * @return <code>true</code> if the credentials need to be reset, <code>false</code> otherwise.
     */
    public boolean isForcedCredentialReset() {
        return m_state == State.FORCED_CREDENTIALS_RESET;
    }

    /**
     * @return <code>true</code> if this account is locked, <code>false</code> otherwise.
     */
    public boolean isLocked() {
        return m_locked;
    }

    /**
     * @return <code>true</code> if the credentials need to be reset, <code>false</code> otherwise.
     */
    public boolean isVoluntaryCredentialReset() {
        return m_state == State.VOLUNTARY_CREDENTIALS_RESET;
    }

    /**
     * Locks this account.
     */
    public void lock() {
        m_locked = true;
    }

    /**
     * @param credentials
     */
    public void setCredentials(Map<String, String> credentials) {
        assertNotLocked();
        m_accessToken = null;
        m_credentials.putAll(credentials);
        m_state = State.NORMAL;
    }

    /**
     * @param resetToken
     */
    public void resetCredentials(String resetToken, boolean forced) {
        assertNotLocked();

        State nextState;
        if (m_state == State.ACCOUNT_VERIFICATION_NEEDED) {
            // This is not what we want!
            throw new IllegalStateException();
        }
        else if (m_state == State.FORCED_CREDENTIALS_RESET) {
            // Ignore change; keep this state as-is...
            nextState = m_state;
        }
        else {
            nextState = forced ? State.FORCED_CREDENTIALS_RESET : State.VOLUNTARY_CREDENTIALS_RESET;
        }

        m_accessToken = Objects.requireNonNull(resetToken);
        m_state = nextState;
    }

    /**
     * Unlocks this account.
     */
    public void unlock() {
        if (isLocked()) {
            m_locked = false;
        }
        else {
            throw new AccountException("Account is not locked!");
        }
    }

    protected final void assertNotLocked() {
        if (isLocked()) {
            throw new AccountLockedException();
        }
    }
}
