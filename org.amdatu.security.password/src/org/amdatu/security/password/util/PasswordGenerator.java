/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.util;

import static java.util.Arrays.asList;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Provides a fairly secure way of generating random passwords of a certain length.
 */
public class PasswordGenerator {

    public static class Builder {
        private SecureRandom m_prng;
        private String m_separator;
        private Set<String> m_alphabet;
        private int m_length;

        public Builder() {
            m_prng = new SecureRandom();
            m_length = DEFAULT_LENGTH;
            m_separator = "";
            setAlphabet(DEFAULT_ALPHABET);
        }

        public PasswordGenerator build() {
            if (m_length < 1 || m_length > 26) {
                throw new IllegalArgumentException("Invalid password length!");
            }
            if (m_alphabet == null || m_alphabet.size() < (2 * m_length)) {
                throw new IllegalArgumentException("Invalid alphabet!");
            }

            try {
                return new PasswordGenerator(m_prng, m_alphabet, m_length, m_separator);
            }
            finally {
                m_alphabet = null;
            }
        }

        public Builder setAlphabet(Collection<String> alphabet) {
            m_alphabet = new HashSet<>(Objects.requireNonNull(alphabet));
            return this;
        }

        public Builder setAlphabet(String... alphabet) {
            return setAlphabet(asList(Objects.requireNonNull(alphabet)));
        }

        public Builder setLength(int length) {
            m_length = length;
            return this;
        }

        public Builder setSecureRandom(SecureRandom prng) {
            m_prng = Objects.requireNonNull(prng);
            return this;
        }

        public Builder setSeparator(String separator) {
            m_separator = Objects.requireNonNull(separator);
            return this;
        }
    }

    /** The default alphabet contains of all case-sensitive alphanumeric characters. */
    private static final String[] DEFAULT_ALPHABET = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
        "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
        "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7",
        "8", "9" };
    /** Using the default alphabet, this will yield roughly 64 bits of entropy in each generated password. */
    private static final int DEFAULT_LENGTH = 11;

    private final SecureRandom m_prng;
    private final String[] m_alphabet;
    private final String m_separator;
    private final int m_length;

    private PasswordGenerator(SecureRandom prng, Set<String> alphabet, int length, String separator) {
        m_prng = prng;
        m_alphabet = alphabet.toArray(new String[alphabet.size()]);
        m_separator = separator;
        m_length = length;
    }

    /**
     * @return a random generated password of the defined length using the defined alphabet.
     */
    public String generatePassword() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m_length; i++) {
            if (i > 0) {
                sb.append(m_separator);
            }
            sb.append(m_alphabet[m_prng.nextInt(m_alphabet.length)]);
        }
        return sb.toString();
    }
}
