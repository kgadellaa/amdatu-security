/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt.impl;

import static org.osgi.service.log.LogService.LOG_WARNING;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.amdatu.security.tokenprovider.jwt.JwtConstants;
import org.amdatu.security.tokenprovider.jwt.JwtKey;
import org.amdatu.security.tokenprovider.jwt.JwtKeyProvider;
import org.apache.felix.dm.Component;
import org.jose4j.jwk.HttpsJwks;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.lang.JoseException;
import org.osgi.service.log.LogService;

/**
 * Adapter for services that register an JWKs URL and issuer name.
 */
public class UrlBasedJwkProvider implements JwtKeyProvider {
    // Managed by Felix DM...
    private volatile Component m_component;
    private volatile LogService m_log;
    // Locally managed...
    private volatile HttpsJwks m_jwks;

    @Override
    public List<JwtKey> getVerificationKeys() {
        List<JwtKey> result = new ArrayList<>();
        try {
            List<JsonWebKey> jwks = m_jwks.getJsonWebKeys();
            for (JsonWebKey jwk : jwks) {
                result.add(new JwtKeyImpl(jwk));
            }
        }
        catch (JoseException | IOException e) {
            m_log.log(LOG_WARNING, "Failed to retrieve remote JSON web keys!", e);
        }
        return result;
    }

    /**
     * Called by Felix DM.
     */
    protected final void set(Map<String, ?> properties, Object service) {
        m_jwks = new HttpsJwks((String) properties.get("url"));

        Dictionary<String, Object> props = m_component.getServiceProperties();
        if (props == null) {
            props = new Hashtable<>();
        }
        if (props.get(JwtConstants.KEY_ISSUER) == null) {
            props.put(JwtConstants.KEY_ISSUER, properties.get("issuer"));
            // Make sure to update the properties!
            m_component.setServiceProperties(props);
        }
    }

    /**
     * Called by Felix DM.
     */
    protected final void unset(Map<String, ?> properties, Object service) {
        m_jwks = null;
    }
}
