/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.builder;

import org.amdatu.security.authorization.EntitySelector;

public class EntitySelectorBuilder extends BuilderBase<EntitySelector> {

    public static EntitySelectorBuilder build() {
        return new EntitySelectorBuilder();
    }

    private EntitySelectorBuilder() {
        super(() -> new EntitySelectorImpl());
    }

    public EntitySelectorBuilder withType(String entityType) {
        return with((EntitySelectorImpl selector) -> selector.entityType = entityType);
    }

    public EntitySelectorBuilder withKey(String key) {
        return with((EntitySelectorImpl selector) -> selector.key = key);
    }

    private static class EntitySelectorImpl implements EntitySelector {
        private String entityType = null;
        private String key = null;

        @Override
        public String getEntityType() {
            return entityType;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        // note: implemented based on method calls, not fields, to maintain contract specified in EntitySelector
        // interface
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((getEntityType() == null) ? 0 : getEntityType().hashCode());
            result = prime * result + ((getKey() == null) ? 0 : getKey().hashCode());
            return result;
        }

        @Override
        // note: implemented based on method calls, not fields, to maintain contract specified in EntitySelector
        // interface
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof EntitySelector)) {
                return false;
            }
            EntitySelector other = (EntitySelector) obj;
            if (getEntityType() == null) {
                if (other.getEntityType() != null) {
                    return false;
                }
            }
            else if (!getEntityType().equals(other.getEntityType())) {
                return false;
            }
            if (getKey() == null) {
                if (other.getKey() != null) {
                    return false;
                }
            }
            else if (!getKey().equals(other.getKey())) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " [key=" + getKey() + ", entityType=" + getEntityType() + "]";
        }
    }

}
