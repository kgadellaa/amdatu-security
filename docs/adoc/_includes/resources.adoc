== Resources

[cols="40%,60%"]
|===
| Tools
	| Location
| Source Code
	| https://bitbucket.org/amdatu/amdatu-security[^]
| Issue Tracking
	| https://amdatu.atlassian.net/browse/AMDATUSEC[^]
| Continuous Build
	| https://bitbucket.org/amdatu/amdatu-security/addon/pipelines/home[^]
|===
