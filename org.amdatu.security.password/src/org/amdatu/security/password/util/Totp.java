/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.util;

import static java.util.Objects.requireNonNull;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.time.Instant;

import javax.crypto.Mac;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

import org.amdatu.security.util.crypto.Subtle;

/**
 * Provides a convenience (time-based) one-time password generator, based on TOTP (RFC6238).
 */
public class Totp {

    public static class Builder {
        private byte[] m_secret;
        private String m_macAlg;
        private long m_t0;
        private long m_timestep;
        private int m_noOfDigits;

        public Builder() {
            m_t0 = 0L;
            m_macAlg = DEFAULT_HMAC_ALG;
            m_noOfDigits = DEFAULT_NO_OF_DIGITS;
            m_timestep = DEFAULT_TIME_STEP;
        }

        public Totp build() {
            if (m_secret == null || m_secret.length < 1) {
                throw new IllegalArgumentException("Invalid secret: cannot be null or empty!");
            }
            if (m_timestep < 1L || m_timestep > 3600L) {
                throw new IllegalArgumentException("Invalid time step: should be between 1 and 3600!");
            }
            if (m_noOfDigits < 1 || m_noOfDigits > 10) {
                throw new IllegalArgumentException("Invalid number of digits: should be between 1 and 10!");
            }

            try {
                return new Totp(m_macAlg, m_secret, m_t0, m_timestep, m_noOfDigits);
            }
            finally {
                m_secret = null;
            }
        }

        public Builder setAlgorithm(String macAlg) {
            m_macAlg = requireNonNull(macAlg);
            return this;
        }

        public Builder setNumberOfDigits(int noOfDigits) {
            m_noOfDigits = noOfDigits;
            return this;
        }

        public Builder setSecret(byte[] secret) {
            m_secret = requireNonNull(secret);
            return this;
        }

        public Builder deriveSecret(String ikm, String info, int length) {
            Hkdf hkdf = new Hkdf.Builder().setInitialKeyingMaterial(ikm.getBytes()).build();
            m_secret = hkdf.deriveKey(info == null ? null : info.getBytes(), length);
            return this;
        }

        public Builder deriveSecret(byte[] ikm, byte[] info, int length) {
            Hkdf hkdf = new Hkdf.Builder().setInitialKeyingMaterial(ikm).build();
            m_secret = hkdf.deriveKey(info, length);
            return this;
        }

        public Builder setTimestampZero(long t0) {
            m_t0 = t0;
            return this;
        }

        public Builder setTimeStep(long timeStep) {
            m_timestep = timeStep;
            return this;
        }
    }

    private static final String DEFAULT_HMAC_ALG = "HmacSHA1";
    private static final long DEFAULT_TIME_STEP = 30L;
    private static final int DEFAULT_NO_OF_DIGITS = 8;

    private final Mac m_mac;
    private final long m_t0;
    private final long m_timestep;
    private final long m_modulus;

    /**
     * Creates a new {@link Totp} instance using a given HMAC algorithm and timestep.
     *
     * @param macAlg the HMAC algorithm to use, for example, "HmacSHA256", cannot be <code>null</code>;
     * @param sharedSecret the secret key that is shared between client and server, and should be at least 16 bytes long;
     * @param t0 "time 0";
     * @param timestep the allowed time-step (in seconds), or the time window that will yield the same codes;
     * @param noOfDigits the maximum number of digits to return in each generated code.
     */
    /* package */ Totp(String macAlg, byte[] sharedSecret, long t0, long timestep, int noOfDigits) {
        m_t0 = t0;
        m_timestep = timestep;
        m_modulus = (long) Math.pow(10, noOfDigits);

        try {
            m_mac = Mac.getInstance(macAlg);
            m_mac.init(new SecretKeySpec(sharedSecret, "RAW"));
        }
        catch (GeneralSecurityException e) {
            throw new RuntimeException("Failed to create MAC instance!", e);
        }
    }

    /**
     * @param challenge the optional bytes to include in the code generation process.
     * @return the generated OTP, as raw bytes.
     */
    public long generateCode(byte... challenge) {
        long now = Instant.now().getEpochSecond();
        return generateCode(now, challenge);
    }

    /**
     * @param timestamp the number of seconds since the epoch to generate the code on;
     * @param challenge the optional bytes to include in the code generation process.
     * @return the generated OTP, as raw bytes.
     */
    public long generateCode(long timestamp, byte... challenge) {
        return truncateRawCode(generateRawCode(timestamp, challenge, null));
    }

    /**
     * @param challenge the optional bytes to include in the code generation process;
     * @param output the output buffer to write the code to, can be <code>null</code> if a new buffer should be created and returned.
     * @return the generated OTP, as raw bytes.
     */
    public byte[] generateRawCode(byte[] challenge, byte[] output) {
        long now = Instant.now().getEpochSecond();
        return generateRawCode(now, challenge, output);
    }

    /**
     * @param timestamp the number of seconds since the epoch to generate the code on;
     * @param challenge the optional bytes to include in the code generation process;
     * @param output the output buffer to write the code to, can be <code>null</code> if a new buffer should be created and returned.
     * @return the generated OTP, as raw bytes.
     */
    public byte[] generateRawCode(long timestamp, byte[] challenge, byte[] output) {
        long t = roundTimestamp(timestamp);
        return internalGenerateRawCode(t, challenge, output);
    }

    /**
     * @return the number of bytes returned in the raw generated codes, &gt; 0.
     */
    public int getMacLength() {
        return m_mac.getMacLength();
    }

    /**
     * Validates whether the given code is valid at the current timestamp ("now").
     *
     * @param code the previously generated OTP to verify, cannot be <code>null</code>;
     * @param challenge the optional bytes that were included in the code generation process
     * @return <code>true</code> if the given code is valid, <code>false</code> otherwise.
     */
    public boolean validateCode(long code, byte... challenge) {
        long now = Instant.now().getEpochSecond();
        return validateCode(code, now, challenge);
    }

    /**
     * Validates whether the given code is valid at the given timestamp.
     *
     * @param code the previously generated OTP to verify, cannot be <code>null</code>;
     * @param timestamp the number of seconds since the epoch to validate the code on;
     * @param challenge the optional bytes that were included in the code generation process
     *
     * @return <code>true</code> if the given code is valid, <code>false</code> otherwise.
     */
    public boolean validateCode(long code, long timestamp, byte... challenge) {
        long actualCode = generateCode(timestamp, challenge);
        return (code ^ actualCode) == 0;
    }

    /**
     * Validates whether the given code is valid at the current timestamp ("now").
     *
     * @param code the previously generated OTP to verify, cannot be <code>null</code>;
     * @param challenge the optional bytes that were included in the code generation process
     * @return <code>true</code> if the given code is valid, <code>false</code> otherwise.
     */
    public boolean validateRawCode(byte[] code, byte... challenge) {
        long now = Instant.now().getEpochSecond();
        return validateRawCode(now, code, challenge);
    }

    /**
     * Validates whether the given code is valid at the given timestamp.
     *
     * @param code the previously generated OTP to verify, cannot be <code>null</code>;
     * @param timestamp the number of seconds since the epoch to validate the code on;
     * @param challenge the optional bytes that were included in the code generation process
     *
     * @return <code>true</code> if the given code is valid, <code>false</code> otherwise.
     */
    public boolean validateRawCode(long timestamp, byte[] code, byte... challenge) {
        final int[] offsets = { 0, -1, 1 };

        boolean result = false;
        long ts = roundTimestamp(timestamp);

        for (int offset : offsets) {
            byte[] actualCode = internalGenerateRawCode(ts + offset, challenge, null);
            if (Subtle.isEqual(actualCode, code)) {
                result = true;
            }
        }

        return result;
    }

    /**
     * @param timestamp the number of seconds since the epoch to generate the code on;
     * @param challenge the optional bytes to include in the code generation process.
     * @return the generated OTP, as raw bytes.
     */
    protected final byte[] internalGenerateRawCode(long timestamp, byte[] challenge, byte[] output) {
        try {
            byte[] msg = ByteBuffer.allocate(8 + challenge.length).putLong(timestamp).put(challenge, 0, challenge.length).array();
            if (output == null) {
                output = new byte[m_mac.getMacLength()];
            }
            m_mac.update(msg);
            m_mac.doFinal(output, 0);
            return output;
        }
        catch (ShortBufferException e) {
            throw new IllegalStateException(String.format("Output buffer too short: need at least %d bytes, got %d bytes!", getMacLength(), output.length));
        }
    }

    /**
     * @return the given timestamp divided by the time-step.
     */
    protected final long roundTimestamp(long timestamp) {
        return Math.floorDiv((timestamp - m_t0), m_timestep);
    }

    /**
     * Truncates a given raw code into an integer value containing at most a given number of digits.
     *
     * @param code the raw generated code to truncate into a single integer value;
     * @param numOfDigits the number of digits to include in the resulting integer value.
     * @return an integer representation of the given raw code, containing at most <tt>numOfDigits</tt> digits.
     */
    protected final long truncateRawCode(byte[] code) {
        int offset = code[code.length - 1] & 0x0f;

        long v = ((code[offset] & 0x7f) << 24) |
            ((code[offset + 1] & 0xff) << 16) |
            ((code[offset + 2] & 0xff) << 8) |
            (code[offset + 3] & 0xff);

        return (long) (v % m_modulus);
    }
}
