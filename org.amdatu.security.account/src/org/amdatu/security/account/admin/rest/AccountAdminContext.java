/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;

import java.io.IOException;
import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.util.URIBuilder;
import org.apache.felix.dm.Component;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.context.ServletContextHelper;

/**
 * Defines the Servlet context helper in which the {@link AccountAdminResource} operates.
 */
public class AccountAdminContext extends ServletContextHelper implements ManagedService {
    public static final String KEY_ACCOUNT_PAGE = "defaultAccountPage";

    private static final String INDEX_HTML = "index.html";
    /** The default context path to use. */
    public static final String DEFAULT_CTX_PATH = "/account";

    private final Bundle m_bundle;
    // Managed by Felix DM...
    private volatile Component m_component;
    private volatile String m_accountPage = INDEX_HTML;

    /**
     * Creates a new {@link AccountAdminContext} instance.
     */
    public AccountAdminContext() {
        m_bundle = FrameworkUtil.getBundle(getClass());
    }

    @Override
    public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || "/".equals(pathInfo)) {
            response.sendRedirect(URIBuilder.of(request).appendToPath(m_accountPage).build().toASCIIString());
        }

        return super.handleSecurity(request, response);
    }

    @Override
    public URL getResource(String name) {
        if (name == null) {
            return null;
        }
        return m_bundle.getResource(name);
    }

    @Override
    public Set<String> getResourcePaths(String path) {
        if (path == null) {
            return null;
        }
        Set<String> result = new LinkedHashSet<String>();
        Enumeration<URL> e = m_bundle.findEntries(path, "*", false /* recurse */);
        if (e != null) {
            while (e.hasMoreElements()) {
                result.add(e.nextElement().getPath());
            }
        }
        return result;
    }

    @Override
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        String accountPage = INDEX_HTML;
        Object ctxPath = DEFAULT_CTX_PATH;
        if (properties != null) {
            ctxPath = properties.get(HTTP_WHITEBOARD_CONTEXT_PATH);
            accountPage = (String) properties.get(KEY_ACCOUNT_PAGE);
        }
        if (accountPage != null) {
            m_accountPage = accountPage;
        }
        updateServiceProperties(m_component, HTTP_WHITEBOARD_CONTEXT_PATH, ctxPath);
    }

    private static void updateServiceProperties(Component comp, Object... props) {
        Dictionary<Object, Object> serviceProperties = comp.getServiceProperties();
        for (int i = 0; i < props.length; i += 2) {
            serviceProperties.put(props[i].toString(), props[i + 1]);
        }
        comp.setServiceProperties(serviceProperties);
    }
}
