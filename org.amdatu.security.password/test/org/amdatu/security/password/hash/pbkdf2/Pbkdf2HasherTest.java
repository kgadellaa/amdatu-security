/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.hash.pbkdf2;

import static org.amdatu.security.password.hash.pbkdf2.Pbkdf2Config.KEY_HASH_SIZE;
import static org.amdatu.security.password.hash.pbkdf2.Pbkdf2Config.KEY_ITERATIONS;
import static org.amdatu.security.password.hash.pbkdf2.Pbkdf2Config.KEY_PRNG_ALGORITHM;
import static org.amdatu.security.password.hash.pbkdf2.Pbkdf2Config.KEY_SALT_SIZE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Dictionary;
import java.util.Hashtable;

import org.junit.Test;

/**
 * Test cases for {@link Pbkdf2Hasher}.
 */
public class Pbkdf2HasherTest {

    @Test
    public void testHashPasswordOk() throws Exception {
        Pbkdf2Hasher hasher = new Pbkdf2Hasher();

        String hashed = hasher.hash("secret");
        assertTrue(hasher.verify(hashed, "secret"));

        assertFalse(hasher.verify(hashed, "password"));
    }

    @Test
    public void testVerifyPasswordOk() throws Exception {
        Pbkdf2Hasher hasher = new Pbkdf2Hasher();

        String hashed = hasher.hash("secret");
        assertTrue(hasher.verify(hashed, "secret"));
    }

    @Test
    public void testVerifyPassword_OtherConfiguration() throws Exception {
        Pbkdf2Hasher hasher = new Pbkdf2Hasher();

        String hash1 = hasher.hash("secret");

        Dictionary<String, String> props = new Hashtable<>();
        props.put(KEY_SALT_SIZE, "8");
        props.put(KEY_HASH_SIZE, "16");
        props.put(KEY_ITERATIONS, "4000");

        hasher.updated(props);

        String hash2 = hasher.hash("secret");

        // We do not expect the hashes to be equal...
        assertNotEquals(hash1, hash2);

        assertTrue(hasher.verify(hash2, "secret"));
        // We should still be able to verify the "old" hash...
        assertTrue(hasher.verify(hash1, "secret"));
    }

    @Test
    public void testUseDifferentSecureRandomAlgorithm() throws Exception {
        Pbkdf2Hasher hasher = new Pbkdf2Hasher();

        String hash1 = hasher.hash("secret");

        Dictionary<String, String> props = new Hashtable<>();
        props.put(KEY_SALT_SIZE, "8");
        props.put(KEY_HASH_SIZE, "16");
        props.put(KEY_ITERATIONS, "4000");
        props.put(KEY_PRNG_ALGORITHM, "SHA1PRNG");

        hasher.updated(props);

        String hash2 = hasher.hash("secret");

        // We do not expect the hashes to be equal...
        assertNotEquals(hash1, hash2);

        assertTrue(hasher.verify(hash2, "secret"));
        // We should still be able to verify the "old" hash...
        assertTrue(hasher.verify(hash1, "secret"));
    }

    @Test
    public void testVerifyPasswordFail() throws Exception {
        Pbkdf2Hasher hasher = new Pbkdf2Hasher();

        String hashed = hasher.hash("secret");
        assertFalse(hasher.verify(hashed, "password"));
    }

}
