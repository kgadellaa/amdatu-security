/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider;

import java.util.Map;

/**
 * Provides a service for generating tokens that can be used to track entities, such as users.
 * <p>
 * Tokens can be associated with any number of attributes. How these attributes are associated to
 * a token is implementation specific. In case attributes are not encoded in the token itself, the
 * implementation should take care of keeping and maintaining the proper references between tokens
 * and their attributes.<br>
 * To aid security, tokens <b>must</b> be valid for only a limited amount of time. This implies
 * that tokens must expire after a certain time period. How tokens are expiring is up is
 * implementation specific, but could be implemented as an attribute of the token itself.
 * </p>
 * <p>
 * Implementations are allowed to retain the generated tokens in a non-volatile way but are not
 * required to do so (in contrast to the earlier versions of this API). Storing tokens is considered
 * an implementation detail and specific to the implementation itself.
 * </p>
 * <p>
 * This service on its own does <b>not</b> provide any form of authentication or authorization. It
 * merely generates tokens that could be used in <b>addition</b> to an authorization or
 * authentication process.
 * </p>
 */
public interface TokenProvider {

    /**
     * Generates a new token for the specified set of attributes.
     * <p>
     * The given attributes are considered free form and may contain any number of attributes.<br>
     * Implementations may pose restrictions on the provided attributes, like, for example, requiring
     * certain attributes to be available, or reserving certain attributes for its own use. Which
     * attributes are required and/or restricted must be documented by the implementation. Removing
     * attributes is not allowed. In case invalid attributes were found, an
     * {@link IllegalArgumentException} should be thrown to indicate this.
     * </p>
     * <p>
     * The returned token is also considered free form. It could convey information or be completely
     * opaque, depending on the implementation. Implementations should take care that returned tokens
     * can be transmitted across unsecured channels, implying that sensitive information should be
     * either left out, or the transportation across a secure channel should be mandated as part of
     * the usage documentation. Encryption of the token itself is an option, but not considered
     * mandatory for implementations.
     * </p>
     *
     * @param attributes the map with attributes to include in the token. An empty map is permitted, but <code>null</code> is not.
     * @return a generated token, encoded as string, never <code>null</code>.
     * @throws IllegalArgumentException in case the given attributes are incomplete or otherwise not valid (such as <code>null</code>);
     * @throws TokenProviderException in case an error occurred during token generation.
     */
    String generateToken(Map<String, String> attributes) throws IllegalArgumentException, TokenProviderException;

    /**
     * Verifies if a given token is valid.
     * <p>
     * The exact semantics on how a given token is verified is implementation specific and could range
     * from performing database lookups to matching and verifying information encoded in the token
     * itself.<br>
     * Obviously, implementations can perform additional validation checks if needed, such as checking
     * for expiration times or the presence of certain attributes.
     * </p>
     * <p>
     * If a token is verified to be "correct", then this method returns the attributes that are
     * included in or associated to the token.
     * </p>
     *
     * @param token the (encoded) token to verify, cannot be <code>null</code>.
     * @return the map with attributes belonging to the given token if the token was verified correctly.
     *         It never returns <code>null</code>, but an empty map is possible.
     * @throws IllegalArgumentException in case the given token string was <code>null</code>;
     * @throws InvalidTokenException in case the provided token is verified as "not correct";
     * @throws TokenProviderException in case of other errors during the verification of the token.
     */
    Map<String, String> verifyToken(String token) throws IllegalArgumentException, InvalidTokenException, TokenProviderException;

}
