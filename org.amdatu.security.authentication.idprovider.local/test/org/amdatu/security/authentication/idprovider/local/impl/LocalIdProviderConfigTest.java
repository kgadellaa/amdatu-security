/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.local.impl;

import static org.amdatu.security.authentication.idprovider.local.impl.LocalIdProviderConfig.DEFAULT_TIMESTEP_WINDOW;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.security.authentication.idprovider.local.impl.LocalIdProvider;
import org.amdatu.security.authentication.idprovider.local.impl.LocalIdProviderConfig;
import org.junit.Test;

/**
 * Test cases for {@link LocalIdProvider}.
 */
public class LocalIdProviderConfigTest {

    @Test
    public void testGenerateVerifyStateValue() throws Exception {
        LocalIdProviderConfig cfg = new LocalIdProviderConfig(asDictionary(
            "name", "itest",
            "signingKeyUri", "random:32",
            "timestepWindow", Integer.toString(DEFAULT_TIMESTEP_WINDOW)));

        byte[] info1 = new byte[] { 1, 2, 3, 4 };
        byte[] info2 = new byte[] { 4, 5, 6, 7 };

        Instant now = Instant.now().truncatedTo(ChronoUnit.MINUTES);

        String state1 = cfg.generateStateCodeAt(now, info1, "foo@bar.com");
        String state2 = cfg.generateStateCodeAt(now, info2, "qux@bar.com");

        assertEquals("foo@bar.com", cfg.verifyStateCodeAt(now, info1, state1));
        assertEquals("foo@bar.com", cfg.verifyStateCodeAt(now.plusSeconds(DEFAULT_TIMESTEP_WINDOW - 1), info1, state1));

        assertEquals("qux@bar.com", cfg.verifyStateCodeAt(now, info2, state2));

        assertNull(cfg.verifyStateCodeAt(now, info1, state2));
        assertNull(cfg.verifyStateCodeAt(now, info2, state1));
        assertNull(cfg.verifyStateCodeAt(now.plusSeconds(DEFAULT_TIMESTEP_WINDOW), info1, state1));
    }

    private static Dictionary<String, ?> asDictionary(Object... args) {
        Dictionary<String, Object> result = new Hashtable<>();
        for (int i = 0; i < args.length; i += 2) {
            result.put(args[i].toString(), args[i + 1]);
        }
        return result;
    }

}
