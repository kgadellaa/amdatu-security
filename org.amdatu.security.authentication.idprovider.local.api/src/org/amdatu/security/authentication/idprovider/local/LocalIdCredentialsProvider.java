/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.local;

import static java.util.Collections.emptyMap;

import java.util.Map;
import java.util.Optional;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Provides a way to lookup the identity and credentials for a local entity, such as user
 * or computer.
 * <p>
 * This service is <b>not</b> the same as a <tt>PrincipalLookupService</tt>! This service
 * is about looking up entities in some locally managed set, which might be using
 * different identifiers than is used to identify principals!
 */
@ConsumerType
public interface LocalIdCredentialsProvider {

    /**
     * Maps a set of given (private and public) credentials to the identifier of a local
     * entity.
     *
     * @param credentials the credentials as given by the local identity provider,
     *        cannot be <code>null</code>.
     * @return the local identifier, can be empty in case no such local identity exists.
     */
    // tag::getId[]
    Optional<String> getId(Map<String, String> credentials);
    // end::getId[]

    /**
     * Returns the public credentials for a local entity.
     * <p>
     * Public credentials are those that do not expose sensitive information, such as
     * passwords, secret cryptographic keys, and so on.<br>
     * Any additional credentials you want to return as part of the local identity of
     * a principal can be returned by this method. For example, you might want to
     * return the full name of the user, its department or any other information that
     * can be of use later on in the application.
     * </p>
     * <p>
     * This is an optional method, that by default returns an empty {@link Map}.
     * </p>
     *
     * @param localId the local identity as previously returned by {@link #getId(Map)},
     *        cannot be <code>null</code> or empty;
     * @return the public credentials for the entity with the given identifier, can be
     *         empty in case no such local identity exists.
     */
    // tag::getPublicCredentials[]
    default Optional<Map<String, String>> getPublicCredentials(String localId) {
        return Optional.of(emptyMap());
    }
    // end::getPublicCredentials[]

}
