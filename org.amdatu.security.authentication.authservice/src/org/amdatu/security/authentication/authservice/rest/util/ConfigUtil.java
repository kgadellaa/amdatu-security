/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest.util;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.osgi.service.cm.ConfigurationException;

/**
 * Provides a couple of convenience methods to work with the authentication
 * resource configuration.
 */
public final class ConfigUtil {

    private final static String BSN_REGEX = "[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*";
    private final static Pattern BSN_PATTERN = Pattern.compile(BSN_REGEX);

    public static boolean getBoolean(Dictionary<String, ?> dict, String key, boolean dflt)
        throws ConfigurationException {
        String val = getString(dict, key, null);
        if (val == null) {
            return dflt;
        }
        return Boolean.parseBoolean(val);
    }

    public static int getInteger(Dictionary<String, ?> dict, String key, int dflt) throws ConfigurationException {
        String val = getString(dict, key, null);
        if (val == null) {
            return dflt;
        }
        try {
            return Integer.parseInt(val);
        }
        catch (NumberFormatException e) {
            return dflt;
        }
    }

    public static long getLong(Dictionary<String, ?> dict, String key, long dflt) throws ConfigurationException {
        String val = getString(dict, key, null);
        if (val == null) {
            return dflt;
        }
        try {
            return Long.parseLong(val);
        }
        catch (NumberFormatException e) {
            return dflt;
        }
    }

    public static String getMandatoryString(Dictionary<String, ?> dict, String key) throws ConfigurationException {
        String val = getString(dict, key, null);
        if (val == null || "".equals(val)) {
            throw new ConfigurationException(key, "cannot be null or empty!");
        }
        return val;
    }

    public static String getString(Dictionary<String, ?> dict, String key) {
        Object val = dict.get(key);
        if (val == null) {
            return null;
        }
        return val.toString().trim();
    }

    public static String getString(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
        Object val = dict.get(key);
        if (val == null) {
            return dflt;
        }
        if (val instanceof String) {
            return ((String) val).trim();
        }
        throw new ConfigurationException(key, "must be a string!");
    }

    public static List<String> getStringPlus(Dictionary<String, ?> dict, String propertyKey) {
        Object property = dict.get(propertyKey);

        if (property == null) {
            return Collections.emptyList();
        }

        if (property instanceof String) {
            String stringProperty = (String) property;
            return Stream.of(stringProperty.split(","))
                    .map(String::trim)
                    .collect(Collectors.toList());
        }
        else if (property instanceof String[]) {
            return Collections.unmodifiableList(Arrays.asList((String[]) property));
        }
        else if (property instanceof Collection) {
            List<String> result = new ArrayList<>();
            Collection collectionProperty = (Collection) property;
            for (Object o : collectionProperty) {
                if (o instanceof String) {
                    result.add((String) o);
                }
                else {
                    throw new RuntimeException(String.format(
                            "Invalid value for string plus property %s expected String, String[] or Collection<String> " +
                                    "got Collection containing %s", propertyKey, o.getClass()));
                }
            }
            return Collections.unmodifiableList(result);
        }
        else {
            throw new RuntimeException(String.format(
                    "Invalid value for string plus property %s expected String, String[] or Collection<String> got %s",
                    propertyKey, property.getClass()));
        }
    }

    public static URI getURI(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
        String val = getString(dict, key, dflt);
        if (val == null) {
            return null;
        }
        return URI.create(val);
    }

    public static boolean isValidSymbolicName(String name) {
        return BSN_PATTERN.matcher(name).matches();
    }
}
