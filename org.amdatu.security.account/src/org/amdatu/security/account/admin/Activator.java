/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import org.amdatu.security.account.AccountAdmin;
import org.amdatu.security.account.AccountAdminBackend;
import org.amdatu.security.account.AccountValidator;
import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;
import org.amdatu.security.password.hash.PasswordHasher;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
    private static final String PID = "org.amdatu.security.account.admin";

    @Override
    public void init(BundleContext context, DependencyManager dm) {
        Properties accountAdminProps = new Properties();
        accountAdminProps.put("name", "amdatuAccountAdmin");

        dm.add(createComponent()
            .setInterface(AccountAdmin.class.getName(), accountAdminProps)
            .setImplementation(AccountAdminImpl.class)
            .add(createConfigurationDependency().setCallback("updated").setPid(PID))
            .add(createServiceDependency().setService(AccountAdminBackend.class).setRequired(true))
            .add(createServiceDependency().setService(PasswordHasher.class).setRequired(true))
            .add(createServiceDependency().setService(TokenProvider.class).setRequired(true))
            .add(createServiceDependency().setService(AccountValidator.class).setRequired(false))
            .add(createServiceDependency().setService(EventAdmin.class).setRequired(false))
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));

        Dictionary<String, Object> props = new Hashtable<>();
        props.put("name", "amdatuAccountAdmin");

        dm.add(createComponent()
            .setInterface(LocalIdCredentialsProvider.class.getName(), props)
            .setImplementation(AccountCredentialsProvider.class)
            .add(createServiceDependency()
                    .setService(AccountAdmin.class, "(name=amdatuAccountAdmin)")
                    .setRequired(true))
            .add(createConfigurationDependency().setCallback("updated").setPid(PID)));
    }

}
